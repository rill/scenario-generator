module Main where

import           Draw                      (drawPPI)
import           OneStep                   (oneStep)
import           Vessel                    (Vessel)

import           GlobalParameters

import           ParseOptions

import           Control.Monad             (replicateM_)
import           Control.Monad.Reader      (runReaderT)
import           Control.Monad.State.Lazy  (liftIO, runStateT)

import           Test.QuickCheck.Arbitrary (vector)
import           Test.QuickCheck.Gen       (Gen, generate)

import           System.IO                 (IOMode (..), hPutStrLn, withFile)

main = do
    Options outfile numVessels numScans <- parseOptions
    vessels <- generate (vector numVessels :: Gen [Vessel])
    {-drawPPI vessels-}
    {-print vessels-}
    withFile
        outfile
        WriteMode
        (\h -> do
             hPutStrLn h $ "scansPerMinute: " ++ show scansPerMinute
             hPutStrLn h $ "cellSize: " ++ show cellSize
             hPutStrLn h $ "samplesPerSpoke: " ++ show samplesPerSpoke
             hPutStrLn h $ "spokesPerScan: " ++ show spokesPerScan
             runStateT (runReaderT (replicateM_ numScans oneStep) h) vessels)
