module OneStep (oneStep) where

import           GlobalParameters

import           Geometry
import           Vessel

import           Data.Foldable            (traverse_)
import           Tracing                  (pixelToInt, shootBScanRay)

import           Control.Monad.Reader     (ReaderT, ask, runReaderT)
import           Control.Monad.State.Lazy (State, StateT, execState, get,
                                           liftIO, modify, put, runStateT)

import           System.IO                (Handle, hPrint, hPutStrLn)

oneStep :: ReaderT Handle (StateT [Vessel] IO) ()
oneStep = do
    h <- ask
    vessels <- get
    let timeDelta = 60 / fromIntegral scansPerMinute
    modify $ map (positionAfter timeDelta)
    vessels' <- get
    let geoms = getGeometry <$> vessels
        maxBScanAzIndex = spokesPerScan - 1
    liftIO $ do
        hPutStrLn h "--"
        traverse_
            (hPrint h)
            [ pixelToInt <$> shootBScanRay azimuth geoms
            | azimuth <- [0 .. maxBScanAzIndex]
            ]
        putStrLn "scan"
    {-liftIO $ drawPPI vessels'-}

positionAfter :: Double -> Vessel -> Vessel
positionAfter t vessel =
    let Speed s = getSpeed vessel
        Orientation theta = (getOrientation . getGeometry) vessel
        deltaX = s * sin theta * t
        deltaY = s * cos theta * t
        geom = getGeometry vessel
        accel = getAcceleration vessel
        shape = getShape geom
        Position x y = getPosition geom
        orientation = getOrientation geom
        pos' = Position (x + deltaX) (y + deltaY)
        geom' = Geometry shape pos' orientation
    in if s == 0
        then vessel
        else Vessel geom' (Speed s) accel

