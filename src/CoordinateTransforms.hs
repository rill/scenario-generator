module CoordinateTransforms (rotate, translate) where

import           Geometry (Position (..))

rotate :: Double -> Position -> Position
rotate theta (Position x y) = Position x' y'
    where x' = x * cosTheta + y * sinTheta
          y' = y * cosTheta - x * sinTheta
          cosTheta = cos theta
          sinTheta = sin theta

translate :: (Double, Double) -> Position -> Position
translate (deltaX, deltaY) (Position x y) = Position (x + deltaX) (y + deltaY)
