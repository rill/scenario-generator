module Geometry
    ( Geometry(..), Shape(..), Position(..), positionNorm, Orientation(..)
    ) where

import           GlobalParameters

import           Test.QuickCheck.Arbitrary
import           Test.QuickCheck.Gen

data Geometry = Geometry
    { getShape       :: Shape
    , getPosition    :: Position
    , getOrientation :: Orientation -- angle w.r.t. positive Y axis
    } deriving (Show)

data Shape
    = Circle { getRadius :: Double }
    | Rectangle { getLength :: Double
                , getWidth  :: Double }
    | Ellipse { getSemiMajor :: Double
              , getSemiMinor :: Double }
    deriving (Show)

data Position = Position
    { getX :: Double
    , getY :: Double
    } deriving (Show)

instance Eq Position where
    p1 == p2 = abs(getX p1 - getX p2) < 0.001 && abs(getY p1 - getY p2) < 0.001

instance Ord Position where
    p1 <= p2 = positionNorm p1 <= positionNorm p2

positionNorm (Position x y) = sqrt (x^2 + y^2)

newtype Orientation = Orientation Double deriving (Show, Eq)

instance Arbitrary Geometry where
    arbitrary = Geometry <$> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary Shape where
    arbitrary =
        frequency
            [ (1, genArbitraryCircle)
            , (3, genArbitraryRectangle)
            , (6, genArbitraryEllipse)
            ]

genArbitraryCircle :: Gen Shape
genArbitraryCircle = Circle <$> choose (50, 500)

genArbitraryRectangle :: Gen Shape
genArbitraryRectangle = do
    l <- choose (200, 4000)
    minW <- choose (50, 100)
    let w = max (l / 5) minW
    return $ Rectangle l w

genArbitraryEllipse :: Gen Shape
genArbitraryEllipse = do
    semiMajor <- choose (100, 3000)
    let semiMinor = semiMajor / 5
    return $ Ellipse semiMajor semiMinor

instance Arbitrary Position where
    arbitrary = do
        r <- choose (1000 :: Double, maxRange - cellSize)
        theta <- azToBearing <$> choose (0, spokesPerScan)
        return $ Position (r * sin theta) (r * cos theta)
      where
        azToBearing :: Int -> Double
        azToBearing az =
            2.0 * pi * fromIntegral az / fromIntegral spokesPerScan

instance Arbitrary Orientation where
    arbitrary = Orientation <$> choose (0, 2 * pi)
