module Tracing (shootBScanRay, pixelToInt, shootPPIRay) where

import           Geometry
import           GeometryIntersection
import           GlobalParameters

import           Data.Ix
import           Data.List            (sort, sortBy)
import           Data.Maybe           (mapMaybe)

import           Graphics.Image       (Bit, Pixel, X, off, on)

type P = Pixel X Bit
type Index = Int

shootBScanRay :: Int -> [Geometry] -> [((Index, Index), P)]
shootBScanRay azimuth geoms =
    let maxSampleIndex :: Index
        maxSampleIndex = samplesPerSpoke - 1
        theta :: Double
        theta = (fromIntegral azimuth / fromIntegral spokesPerScan) * 2 * pi
        segment =
            ( Position 0 0
            , Position (maxRange * sin theta) (maxRange * cos theta))
        intersectionToIndex :: Position -> Index
        intersectionToIndex p =
            round $ positionNorm p * fromIntegral maxSampleIndex / maxRange
        ranges = intersectionRanges segment geoms intersectionToIndex
    in [ if any (`inRange` y) ranges
           then ((y, azimuth), off)
           else ((y, azimuth), on)
       | y <- [0 .. maxSampleIndex]
       ]

shootPPIRay :: Int -> [Geometry] -> [((Index, Index), P)]
shootPPIRay xIndex geoms =
    let maxIndex :: Index
        maxIndex = 2 * samplesPerSpoke - 1
        segmentX = fromIntegral xIndex * cellSize - maxRange
        segment = (Position segmentX maxRange, Position segmentX (-maxRange))
        intersectionToIndex :: Position -> Index
        intersectionToIndex p =
            round $
            (maxRange - getY p) * fromIntegral samplesPerSpoke /
            maxRange
        ranges = intersectionRanges segment geoms intersectionToIndex
    in [ if any (`inRange` y) ranges
           then ((y, xIndex), off)
           else ((y, xIndex), on)
       | y <- [1 .. (maxIndex - 1)]
       ] ++
       [((0, xIndex), off), ((maxIndex, xIndex), off)]

intersectionRanges ::
       (Position, Position)
    -> [Geometry]
    -> (Position -> Index)
    -> [(Index, Index)]
intersectionRanges segment geoms toIndex =
    mapMaybe (intersectionToIndices toIndex . (segment `intersect`)) geoms
  where
    intersectionToIndices ::
           (Position -> Index) -> [Position] -> Maybe (Index, Index)
    intersectionToIndices toIndex ps =
        if length ps == 2
            then let sortedIndices = sort $ toIndex <$> ps
                     li = head sortedIndices
                     ui = (head . tail) sortedIndices
                 in Just (li, ui)
            else Nothing

pixelToInt :: ((Index, Index), P) -> Int
pixelToInt ((_, _), p)
    | p == on = 0
    | otherwise = 1
