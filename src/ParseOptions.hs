module ParseOptions (parseOptions, Options(..)) where

import           Data.Semigroup      ((<>))
import           Options.Applicative

data Options = Options
    { getOutputFile :: FilePath
    , getNumVessels :: Int
    , getNumScans   :: Int
    } deriving (Show)

optionsInfo :: Parser Options
optionsInfo =
    Options <$>
    strOption
        (long "output-file" <> short 'o' <> metavar "FilePath" <>
         help "Path to the output BScan file (in text format).") <*>
    option auto
        (long "num-vessels" <> short 'v' <> metavar "INT" <>
         value 100 <>
         help "Number of vessels to simulate.") <*>
    option auto
        (long "num-scans" <> short 's' <> metavar "INT" <>
         value 100 <>
         help "Number of scans to simulate")

parseOptions :: IO Options
parseOptions =
    let opts =
            info
                (optionsInfo <**> helper)
                (fullDesc <>
                 progDesc
                     "This program generates BScans by simulating an arbitrary \
                     \world of vessels." <>
                 header "scenario generator")
     in execParser opts
