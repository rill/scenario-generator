module GeometryIntersection
    ( intersect
    ) where

import           CoordinateTransforms
import           Geometry             (Geometry (..), Orientation (..),
                                       Position (..), Shape (..))
import           QuadraticRoots       (realRoots)

import           Data.Maybe           (catMaybes)

-- compute the points of intersection between a line segment and a shape
intersect :: (Position, Position) -> Geometry -> [Position]
intersect (p1, p2) obj =
    case getShape obj of
        Circle r -> intersectLineSegmentWithCircle (p1, p2) p r
        Rectangle l w ->
            map xform' $ intersectLineSegmentWithRectangle (p1', p2') l w
        Ellipse a b ->
            map xform' $ intersectLineSegmentWithEllipse (p1', p2') a b
  where
    p = getPosition obj
    Orientation theta = getOrientation obj
    originXform :: Position -> Double -> Position -> Position
    originXform p theta = rotate (-theta) . translate (-xc, -yc)
      where
        Position xc yc = p
    reverseXform :: Position -> Double -> Position -> Position
    reverseXform p theta = translate (xc, yc) . rotate theta
      where
        Position xc yc = p
    xform :: Position -> Position
    xform = originXform p theta
    xform' :: Position -> Position
    xform' = reverseXform p theta
    p1' = xform p1
    p2' = xform p2

intersectLineSegmentWithCircle (Position x1 y1, Position x2 y2) pr@(Position xr yr) r =
    intersections
  where
    deltaX = x2 - x1
    deltaY = y2 - y1
    a = deltaX ^ 2 + deltaY ^ 2
    b = 2 * (x1 * deltaX + y1 * deltaY - xr * deltaX - yr * deltaY)
    c = x1 ^ 2 + y1 ^ 2 + xr ^ 2 + yr ^ 2 - 2 * x1 * xr - 2 * y1 * yr - r ^ 2
    roots = filter (\t -> (t >= 0) && (t <= 1)) $ realRoots a b c
    parameterizedLocation t = Position (x1 + t * (x2 - x1)) (y1 + t * (y2 - y1))
    intersections = parameterizedLocation <$> roots

intersectLineSegmentWithEllipse (Position x1 y1, Position x2 y2) a b =
    intersections
  where
    deltaX = x2 - x1
    deltaY = y2 - y1
    coeffA = a ^ 2 * deltaX ^ 2 + b ^ 2 * deltaY ^ 2
    coeffB = 2 * (a ^ 2 * x1 * deltaX + b ^ 2 * y1 * deltaY)
    coeffC = a ^ 2 * x1 ^ 2 + b ^ 2 * y1 ^ 2 - a ^ 2 * b ^ 2
    roots = filter (\t -> (t >= 0) && (t <= 1)) $ realRoots coeffA coeffB coeffC
    parameterizedLocation t = Position (x1 + t * (x2 - x1)) (y1 + t * (y2 - y1))
    intersections = parameterizedLocation <$> roots

intersectLineSegmentWithRectangle (p1, p2) l w =
    if allVerticesOnSameSide
        then []
        else intersections
  where
    vertices =
        [ Position (-w / 2) (-l / 2)
        , Position (-w / 2) (l / 2)
        , Position (w / 2) (l / 2)
        , Position (w / 2) (-l / 2)
        ]
    intersections =
        catMaybes $
        intersectLineSegments (p1, p2) <$>
        [ (head vertices, vertices !! 1)
        , (vertices !! 1, vertices !! 2)
        , (vertices !! 2, vertices !! 3)
        , (vertices !! 3, head vertices)
        ]
    allVerticesOnSameSide = all (== True) signs || all (== False) signs
      where
        signs = map (sign p1 p2) vertices
        -- do a cross-product to test on which side a point lies w.r.t. a line
        -- points on the same side of a line should return the same sign
        sign :: Position -> Position -> Position -> Bool
        sign (Position x1 y1) (Position x2 y2) (Position x y) =
            signum ((y2 - y1) * x + (x1 - x2) * y + (x2 * y1 - x1 * y2)) == 1.0
    intersectLineSegments ::
           (Position, Position) -> (Position, Position) -> Maybe Position
    intersectLineSegments (Position x1 y1, Position x1' y1') (Position x2 y2, Position x2' y2') =
        if haveSolution
            then Just $ Position (x1 + t1 * deltaX1) (y1 + t1 * deltaY1)
            else Nothing
      where
        deltaX1 = x1' - x1
        deltaY1 = y1' - y1
        deltaX2 = x2' - x2
        deltaY2 = y2' - y2
        denominator = deltaX1 * deltaY2 - deltaX2 * deltaY1
        t1 = (deltaX2 * (y1 - y2) + deltaY2 * (x2 - x1)) / denominator
        t2 = (deltaX1 * (y1 - y2) + deltaY1 * (x2 - x1)) / denominator
        haveSolution =
            (denominator /= 0) &&
            (t1 >= 0) && (t1 <= 1) && (t2 >= 0) && (t2 <= 1)
