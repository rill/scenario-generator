module GlobalParameters where

scansPerMinute :: Int
scansPerMinute = 48

cellSize :: Double
cellSize = 108.5

samplesPerSpoke :: Int
samplesPerSpoke = 4096

spokesPerScan :: Int
spokesPerScan = 2048

maxRange :: Double
maxRange = cellSize * fromIntegral (samplesPerSpoke - 1)
