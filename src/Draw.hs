module Draw (drawPPI, drawBScan) where

import           Data.Array

import           Geometry
import           GlobalParameters
import           Vessel

import           Tracing

import           Graphics.Image   (Bit, Pixel, VU (..), X, displayImage,
                                   fromListsR, makeImageR, off, on)

drawPPI :: [Vessel] -> IO ()
drawPPI vessels = do
    let geoms = getGeometry <$> vessels
        maxPPIIndex :: Int
        maxPPIIndex = 2 * samplesPerSpoke - 1
        ppiPixels =
            array ((0, 0), (maxPPIIndex, maxPPIIndex)) $
            concat [shootPPIRay xIndex geoms | xIndex <- [0 .. maxPPIIndex]]
        ppi =
                makeImageR
                    VU
                    (2 * samplesPerSpoke, 2 * samplesPerSpoke)
                    (\(i, j) -> ppiPixels ! (i, j))
    displayImage ppi

drawBScan :: [Vessel] -> IO ()
drawBScan vessels = do
    let geoms = getGeometry <$> vessels
        maxBScanAzIndex = spokesPerScan - 1
        bscanPixels =
            array ((0, 0), (samplesPerSpoke, maxBScanAzIndex)) $
            concat
                [ shootBScanRay azimuth geoms
                | azimuth <- [0 .. maxBScanAzIndex]
                ]
        bscan =
            makeImageR
                VU
                (samplesPerSpoke, maxBScanAzIndex)
                (\(i, j) -> bscanPixels ! (i, j))
    displayImage bscan

