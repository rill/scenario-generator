module QuadraticRoots
    ( roots
    , realRoots
    ) where

import           Data.Complex

-- roots for quadratic equations with complex coefficients
croots :: (RealFloat a) => Complex a -> Complex a -> Complex a -> [Complex a]
croots a b c
    | disc == 0 = [solution (+)]
    | otherwise = [solution (+), solution (-)]
  where
    disc = b * b - 4 * a * c
    solution plmi = plmi (-b) (sqrt disc) / (2 * a)

-- roots for quadratic equations with real coefficients
roots :: (RealFloat a) => a -> a -> a -> [Complex a]
roots a b c = croots (a :+ 0) (b :+ 0) (c :+ 0)

realRoots a b c =
    map realPart . filter (\r -> imagPart r == 0) $ roots a b c
