module Vessel
    ( Vessel(..), Speed(..)
    ) where

import           Geometry

import           Test.QuickCheck.Arbitrary
import           Test.QuickCheck.Gen

data Vessel = Vessel
    { getGeometry     :: Geometry
    , getSpeed        :: Speed
    , getAcceleration :: Acceleration
    } deriving (Show)

newtype Speed = Speed Double deriving (Show, Eq)

data Acceleration = Acceleration
    { getAX :: Double
    , getAY :: Double
    } deriving (Show)

instance Arbitrary Vessel where
    arbitrary = do
        geom <- arbitrary
        speed <- case getShape geom of
            Circle _      -> pure (Speed 0)
            Rectangle _ _ -> Speed <$> choose (0, 50) -- up to 10kn/hr
            Ellipse _ _   -> Speed <$> choose (0, 150) -- up to 30kn/hr
        Vessel <$> pure geom <*> pure speed <*> pure (Acceleration 0 0)
