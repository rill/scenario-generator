module RectangleIntersectionSpec where

import           Geometry
import           GeometryIntersection

import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck

import           Debug.Trace

spec =
    describe "intersection of line segment with rectangle" $
    context "rectange and p1 at origin" $ do
        it "p2 at (1000,0)" $ do
            let p1 = Position 0 0
                p2 = Position 1000 0
                rect =
                    Geometry (Rectangle 100 20) (Position 0 0) (Orientation 0)
            intersect (p1, p2) rect `shouldBe` [Position 10 0]
        it "should intersect p2 at (10,0)" $ do
            let p1 = Position 0 0
                p2 = Position 10 0
                rect =
                    Geometry (Rectangle 100 20) (Position 0 0) (Orientation 0)
            intersect (p1, p2) rect `shouldBe` [Position 10 0]
        it "should not intersect p2 at (5,0)" $ do
            let p1 = Position 0 0
                p2 = Position 5 0
                rect =
                    Geometry (Rectangle 100 20) (Position 0 0) (Orientation 0)
            intersect (p1, p2) rect `shouldBe` []
        it "should intersect p2 at (1000,0) with orientation pi/2" $ do
            let p1 = Position 0 0
                p2 = Position 1000 0
                rect =
                    Geometry
                        (Rectangle 100 20)
                        (Position 0 0)
                        (Orientation (pi / 2))
                matchesExpectation [Position x y] =
                    abs (x - 50) < 0.001 && abs y < 0.001
            matchesExpectation ((p1, p2) `intersect` rect) `shouldBe` True
        it
            "intersect arbitrary horizontal line (-1000,y)--(1000,y) with rect@(0,0),100,20,0" $
            property $
            forAll (oneof [choose (-100, -50), choose (50, 100)]) $ \y ->
                let p1 = Position (-1000) y
                    p2 = Position 1000 y
                    rect =
                        Geometry
                            (Rectangle 100 20)
                            (Position 0 0)
                            (Orientation 0)
                    intersections = (p1, p2) `intersect` rect
                 in null intersections
        it
            "intersect arbitrary horizontal line (-1000,y)--(1000,y) with rect@(0,0),100,20,0" $
            property $
            forAll (choose (-50, 50)) $ \y ->
                let p1 = Position (-1000) y
                    p2 = Position 1000 y
                    rect =
                        Geometry
                            (Rectangle 100 20)
                            (Position 0 0)
                            (Orientation 0)
                    intersections = (p1, p2) `intersect` rect
                 in length intersections == 2 &&
                    all (`elem` [Position (-10) y, Position 10 y]) intersections
        it
            "intersect arbitrary vertical line (x, -1000)--(x, 1000) with rect@(0,0),100,20,0" $
            property $
            forAll (choose (-10, 10)) $ \x ->
                let p1 = Position x (-1000)
                    p2 = Position x 1000
                    rect =
                        Geometry
                            (Rectangle 100 20)
                            (Position 0 0)
                            (Orientation 0)
                    intersections = (p1, p2) `intersect` rect
                 in length intersections == 2 &&
                    all (`elem` [Position x (-50), Position x 50]) intersections
        it
            "intersect arbitrary vertical line (x, -1000)--(x, 1000) with rect@(0,0),100,20,0" $
            property $
            forAll (oneof [choose (-100, -10), choose (10, 100)]) $ \x ->
                let p1 = Position x (-1000)
                    p2 = Position x 1000
                    rect =
                        Geometry
                            (Rectangle 100 20)
                            (Position 0 0)
                            (Orientation 0)
                    intersections = (p1, p2) `intersect` rect
                 in null intersections
        it
            "intersect arbitrary horizontal line (-1000,y)--(1000,y) with rect@(100,100),100,20,(pi/2)" $
            property $
            forAll (choose (-200, 200)) $ \y ->
                let p1 = Position (-1000) y
                    p2 = Position 1000 y
                    rect =
                        Geometry
                            (Rectangle 100 20)
                            (Position 100 100)
                            (Orientation (pi / 2))
                    intersections = (p1, p2) `intersect` rect
                 in if (y >= 90) && (y <= 110)
                        then length intersections == 2 &&
                             all
                                 (`elem` [Position 50 y, Position 150 y])
                                 intersections
                        else null intersections
        it
            "intersect arbitrary horizontal line (-1000,y)--(1000,y) with rect@(100,100),100,20,(3pi/2)" $
            property $
            forAll (choose (-200, 200)) $ \y ->
                let p1 = Position (-1000) y
                    p2 = Position 1000 y
                    rect =
                        Geometry
                            (Rectangle 100 20)
                            (Position 100 100)
                            (Orientation (3 * pi / 2))
                    intersections = (p1, p2) `intersect` rect
                 in if (y >= 90) && (y <= 110)
                        then length intersections == 2 &&
                             all
                                 (`elem` [Position 50 y, Position 150 y])
                                 intersections
                        else null intersections
        it "intersect arbitrary vertical line (x, -1000)--(x, 1000) with rect@(100,100),100,20,(pi/2)" $
            property $
            forAll (choose (-200, 200)) $ \x ->
                let p1 = Position x (-1000)
                    p2 = Position x 1000
                    rect =
                        Geometry
                            (Rectangle 100 20)
                            (Position 100 100)
                            (Orientation (pi/2))
                    intersections = (p1, p2) `intersect` rect
                 in if (x >= 50) && (x <= 150)
                        then length intersections == 2 &&
                                all
                                    (`elem` [Position x 90, Position x 110])
                                    intersections
                        else null intersections
        it "intersect arbitrary vertical line (x, -1000)--(x, 1000) with rect@(100,100),100,20,(3*pi/2)" $
            property $
            forAll (choose (-200, 200)) $ \x ->
                let p1 = Position x (-1000)
                    p2 = Position x 1000
                    rect =
                        Geometry
                            (Rectangle 100 20)
                            (Position 100 100)
                            (Orientation (3*pi/2))
                    intersections = (p1, p2) `intersect` rect
                 in if (x >= 50) && (x <= 150)
                        then length intersections == 2 &&
                                all
                                    (`elem` [Position x 90, Position x 110])
                                    intersections
                        else null intersections
        modifyMaxSuccess (const 100000) $
            it "intersect arbitrary lines from origin with rect@(100,100),100,20,(pi/4)" $
                property $
                forAll (choose (0, pi/2)) $ \theta ->
                    let p1 = Position 0 0
                        p2 = Position (1000 * sin theta) (1000 * cos theta)
                        rect =
                            Geometry
                                (Rectangle 100 20)
                                (Position 100 100)
                                (Orientation (pi/4))
                        intersections = (p1, p2) `intersect` rect
                        r2 = sqrt 2
                        theta1 = atan2 (100 - (50/r2) - (10/r2)) (100 - (50/r2) + (10/r2))
                        theta2 = atan2 (100 - (50/r2) + (10/r2)) (100 - (50/r2) - (10/r2))
                    in if (theta >= theta1) && (theta <= theta2)
                            then length intersections == 2
                            else null intersections
        modifyMaxSuccess (const 10000) $
            it "intersect arbitrary lines from origin with rect@(100,100),100,20,(5*pi/4)" $
                property $
                forAll (choose (0, pi/2)) $ \theta ->
                    let p1 = Position 0 0
                        p2 = Position (1000 * sin theta) (1000 * cos theta)
                        rect =
                            Geometry
                                (Rectangle 100 20)
                                (Position 100 100)
                                (Orientation (5*pi/4))
                        intersections = (p1, p2) `intersect` rect
                        r2 = sqrt 2
                        theta1 = atan2 (100 - (50/r2) - (10/r2)) (100 - (50/r2) + (10/r2))
                        theta2 = atan2 (100 - (50/r2) + (10/r2)) (100 - (50/r2) - (10/r2))
                    in if (theta >= theta1) && (theta <= theta2)
                            then length intersections == 2
                            else null intersections
