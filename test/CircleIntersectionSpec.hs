module CircleIntersectionSpec where

import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck

import           Geometry
import           GeometryIntersection

import           Debug.Trace

spec =
    describe "intersection of line segment with circle" $ do
        context "circle and p1 at origin" $ do
            it "p2 at (1000,0)" $ do
                let p1 = Position 0 0
                    p2 = Position 1000 0
                    circle =
                        Geometry (Circle 100) (Position 0 0) (Orientation 0)
                intersect (p1, p2) circle `shouldBe` [Position 100 0]
            it "should not intersect p2 at (10,0)" $ do
                let p1 = Position 0 0
                    p2 = Position 10 0
                    circle =
                        Geometry (Circle 100) (Position 0 0) (Orientation 0)
                intersect (p1, p2) circle `shouldBe` []
            modifyMaxSuccess (const 10000) $
                modifyMaxSize (const 1000) $
                it "intersect arbitrary (0,0)--(x,y) with circle@(0,0),10000" $
                property $ do
                    let p1 = Position 0 0
                        radius = 10000
                        circle =
                            Geometry
                                (Circle radius)
                                (Position 0 0)
                                (Orientation 0)
                    \p2 ->
                        let withinCircle (Position x y) =
                                sqrt (x ^ 2 + y ^ 2) < radius
                            intersections = (p1, p2) `intersect` circle
                            Position ix iy = head intersections
                         in if withinCircle p2
                                then null intersections
                                else (length intersections == 1) &&
                                     (abs (sqrt (ix ^ 2 + iy ^ 2) - radius) <
                                      0.01)
        context "p1 at origin, circle centered along x axis" $ do
            it "p2@(1000,0) circle@(100,0),100" $ do
                let p1 = Position 0 0
                    p2 = Position 1000 0
                    circle =
                        Geometry (Circle 100) (Position 100 0) (Orientation 0)
                    intersections = (p1, p2) `intersect` circle
                all
                    (\p -> p == Position 0 0 || p == Position 200 0)
                    intersections `shouldBe`
                    True
            it "p2@(1000,0) circle@(200,0),100" $ do
                let p1 = Position 0 0
                    p2 = Position 1000 0
                    circle =
                        Geometry (Circle 100) (Position 200 0) (Orientation 0)
                    intersections = (p1, p2) `intersect` circle
                all
                    (\p -> p == Position 100 0 || p == Position 300 0)
                    intersections `shouldBe`
                    True
        context "p1 at origin, circle centered along y axis" $ do
            it "p2@(0,1000) circle@(0,100),100" $ do
                let p1 = Position 0 0
                    p2 = Position 0 1000
                    circle =
                        Geometry (Circle 100) (Position 0 100) (Orientation 0)
                    intersections = (p1, p2) `intersect` circle
                all
                    (\p -> p == Position 0 0 || p == Position 0 200)
                    intersections `shouldBe`
                    True
            it "p2@(0,1000) circle@(0,200),100" $ do
                let p1 = Position 0 0
                    p2 = Position 0 1000
                    circle =
                        Geometry (Circle 100) (Position 0 200) (Orientation 0)
                    intersections = (p1, p2) `intersect` circle
                all
                    (\p -> p == Position 0 100 || p == Position 0 300)
                    intersections `shouldBe`
                    True
        context "random line segment with circle@(2000,0),100" $ do
            modifyMaxSuccess (const 10000) $
                it "random horizontal line" $
                property $ \y ->
                    let p1 = Position 0 y
                        p2 = Position 10000 y
                        radius = 200
                        circle =
                            Geometry
                                (Circle radius)
                                (Position 2000 0)
                                (Orientation 0)
                        intersections = (p1, p2) `intersect` circle
                        pointOnCircle (Position ix iy) =
                            (abs (sqrt ((ix - 2000) ^ 2 + iy ^ 2) - radius) <
                             0.01)
                     in if y > radius || y < (-radius)
                            then null intersections
                            else if y == radius || y == -radius
                                     then length intersections == 1 &&
                                          all pointOnCircle intersections
                                     else length intersections == 2 &&
                                          all pointOnCircle intersections
            modifyMaxSuccess (const 10000) $
                it "random vertical line" $
                property $ \x ->
                    let p1 = Position x 10000
                        p2 = Position x (-10000)
                        radius = 100
                        circle =
                            Geometry
                                (Circle radius)
                                (Position 1000 0)
                                (Orientation 0)
                        intersections = (p1, p2) `intersect` circle
                        pointOnCircle (Position ix iy) =
                            (abs (sqrt ((ix - 1000) ^ 2 + iy ^ 2) - radius) <
                             0.01)
                     in if x < 900 || x > 1100
                            then null intersections
                            else if x == 900 || x == 1100
                                     then length intersections == 1 &&
                                          all pointOnCircle intersections
                                     else length intersections == 2 &&
                                          all pointOnCircle intersections
        context "random line segment from origin to circle@(1000,1000),1000" $
            it "random line" $
            property $ \o ->
                let Orientation theta = o
                    p1 = Position 0 0
                    p2 =
                        Position
                            (10 * radius * sin theta)
                            (10 * radius * cos theta)
                    radius = 1000
                    circle =
                        Geometry
                            (Circle radius)
                            (Position radius radius)
                            (Orientation 0)
                    intersections = (p1, p2) `intersect` circle
                    pointOnCircle (Position ix iy) =
                        (abs (sqrt ((ix - 1000) ^ 2 + (iy - 1000) ^ 2) - radius) <
                         0.01)
                in if theta > (pi / 2)
                        then null intersections
                        else if theta == 0 || theta == (pi / 2)
                                then length intersections == 1 &&
                                        all pointOnCircle intersections
                                else length intersections == 2 &&
                                        all pointOnCircle intersections
