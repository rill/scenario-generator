import           Test.Hspec

import qualified CircleIntersectionSpec
import qualified EllipseIntersectionSpec
import qualified RectangleIntersectionSpec

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "intersection with circle" CircleIntersectionSpec.spec
  describe "intersection with rectangle" RectangleIntersectionSpec.spec
  describe "intersection with ellipse" EllipseIntersectionSpec.spec
