module EllipseIntersectionSpec where

import           Geometry
import           GeometryIntersection

import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck

import           Debug.Trace

spec =
    context "ellipse and p1 at origin" $ do
        it "p2 at (1000,0), ellipse@(0,0)100,20,0" $ do
            let p1 = Position 0 0
                p2 = Position 1000 0
                ell = Geometry (Ellipse 100 20) (Position 0 0) (Orientation 0)
            intersect (p1, p2) ell `shouldBe` [Position 20 0]
        it "p2 at (1000,0), ellipse@(0,0)100,20,(pi/2)" $ do
            let p1 = Position 0 0
                p2 = Position 1000 0
                ell =
                    Geometry
                        (Ellipse 100 20)
                        (Position 0 0)
                        (Orientation (pi / 2))
            intersect (p1, p2) ell `shouldBe` [Position 100 0]
        it "p2 at (1000,0), ellipse@(0,0)100,20,(3pi/2)" $ do
            let p1 = Position 0 0
                p2 = Position 1000 0
                ell =
                    Geometry
                        (Ellipse 100 20)
                        (Position 0 0)
                        (Orientation (3 * pi / 2))
            intersect (p1, p2) ell `shouldBe` [Position 100 0]
        it "p2 at (-1000,0), ellipse@(0,0)100,20,(3pi/2)" $ do
            let p1 = Position 0 0
                p2 = Position (-1000) 0
                ell =
                    Geometry
                        (Ellipse 100 20)
                        (Position 0 0)
                        (Orientation (3 * pi / 2))
            intersect (p1, p2) ell `shouldBe` [Position (-100) 0]
        it "p2 at (0,-1000), ellipse@(0,0)100,20,(3pi/2)" $ do
            let p1 = Position 0 0
                p2 = Position 0 (-1000)
                ell =
                    Geometry
                        (Ellipse 100 20)
                        (Position 0 0)
                        (Orientation (3 * pi / 2))
            intersect (p1, p2) ell `shouldBe` [Position 0 (-20)]
        it "intersect arbitrary line from origin with ellipse@(0,0),100,20,0)" $
            property $
            forAll (choose (0, 2 * pi)) $ \theta ->
                let p1 = Position 0 0
                    p2 = Position (10000 * sin theta) (10000 * cos theta)
                    halfA = 100
                    halfB = 20
                    ell =
                        Geometry
                            (Ellipse halfA halfB)
                            (Position 0 0)
                            (Orientation 0)
                    intersections = (p1, p2) `intersect` ell
                    Position ix iy = head intersections
                 in length intersections == 1 &&
                    abs ((iy ^ 2 / halfA ^ 2) + (ix ^ 2 / halfB ^ 2) - 1) <
                    0.001
        it
            "intersect arbitrary line from origin with ellipse@(0,0),100,20,pi/2)" $
            property $
            forAll (choose (0, 2 * pi)) $ \theta ->
                let p1 = Position 0 0
                    p2 = Position (10000 * sin theta) (10000 * cos theta)
                    halfA = 100
                    halfB = 20
                    ell =
                        Geometry
                            (Ellipse halfA halfB)
                            (Position 0 0)
                            (Orientation (pi / 2))
                    intersections = (p1, p2) `intersect` ell
                    Position ix iy = head intersections
                 in length intersections == 1 &&
                    abs ((iy ^ 2 / halfB ^ 2) + (ix ^ 2 / halfA ^ 2) - 1) <
                    0.001
        it
            "intersect arbitrary line from origin with ellipse@(0,0),100,20,3*pi/2)" $
            property $
            forAll (choose (0, 2 * pi)) $ \theta ->
                let p1 = Position 0 0
                    p2 = Position (10000 * sin theta) (10000 * cos theta)
                    halfA = 100
                    halfB = 20
                    ell =
                        Geometry
                            (Ellipse halfA halfB)
                            (Position 0 0)
                            (Orientation (3 * pi / 2))
                    intersections = (p1, p2) `intersect` ell
                    Position ix iy = head intersections
                 in length intersections == 1 &&
                    abs ((iy ^ 2 / halfB ^ 2) + (ix ^ 2 / halfA ^ 2) - 1) <
                    0.001
        it
            "intersect arbitrary line passing through origin with ellipse@(0,0),100,20,3*pi/2" $
            property $
            forAll (choose (0, 2 * pi)) $ \theta ->
                let x1 = (10000 * sin theta)
                    y1 = (10000 * cos theta)
                    p1 = Position (-x1) (-y1)
                    p2 = Position x1 y1
                    halfA = 100
                    halfB = 20
                    ell =
                        Geometry
                            (Ellipse halfA halfB)
                            (Position 0 0)
                            (Orientation (3 * pi / 2))
                    intersections = (p1, p2) `intersect` ell
                    isPositionOnEclipse (Position ix iy) =
                        abs ((iy ^ 2 / halfB ^ 2) + (ix ^ 2 / halfA ^ 2) - 1) <
                        0.001
                 in length intersections == 2 &&
                    all isPositionOnEclipse intersections
        modifyMaxSuccess (const 10000) $
            it
                "intersect arbitrary vertical line with ellipse@(1000,1000),100,20,3*pi/2" $
                property $
                forAll (choose (0, 2000)) $ \x ->
                    let p1 = Position x (-1000)
                        p2 = Position x 2000
                        halfA = 100
                        halfB = 20
                        ell =
                            Geometry
                                (Ellipse halfA halfB)
                                (Position 1000 1000)
                                (Orientation (3 * pi / 2))
                        intersections = (p1, p2) `intersect` ell
                        isPositionOnEclipse (Position ix iy) =
                            abs
                                (((iy - 1000) ^ 2 / halfB ^ 2) +
                                ((ix - 1000) ^ 2 / halfA ^ 2) -
                                1) <
                            0.001
                    in if (x >= 900) && (x <= 1100)
                        then length intersections == 2 &&
                                all isPositionOnEclipse intersections
                        else null intersections
        modifyMaxSuccess (const 10000) $
            it
                "intersect arbitrary horizontal line with ellipse@(1000,1000),100,20,3*pi/2" $
                property $
                forAll (choose (0, 2000)) $ \y ->
                    let p1 = Position (-1000) y
                        p2 = Position 2000 y
                        halfA = 100
                        halfB = 20
                        ell =
                            Geometry
                                (Ellipse halfA halfB)
                                (Position 1000 1000)
                                (Orientation (3 * pi / 2))
                        intersections = (p1, p2) `intersect` ell
                        isPositionOnEclipse (Position ix iy) =
                            abs
                                (((iy - 1000) ^ 2 / halfB ^ 2) +
                                ((ix - 1000) ^ 2 / halfA ^ 2) -
                                1) <
                            0.001
                    in if (y >= 980) && (y <= 1020)
                        then length intersections == 2 &&
                                all isPositionOnEclipse intersections
                        else null intersections
